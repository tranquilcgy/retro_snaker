#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include <windows.h>
#define  high 25
#define  width 35  //游戏画面大小
#define  enemynum 5  //敌机数目
// 全局变量

int  movedirection ;//小蛇移动方向1，2，3，4分别表示上下左右
int canvas[high][width]={0};//二位数组游戏画面对应元素
                         //0输出空格 1输出蛇头 >1蛇身* -1为边框 -2为food
int food_x,food_y;   //记录food位置



void gotoxy(int x,int y)  //光标移动到(x,y)位置
{
    HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
    COORD pos;
    pos.X = x;
    pos.Y = y;
    SetConsoleCursorPosition(handle,pos);
}

void HideCursor() // 用于隐藏光标
{
    CONSOLE_CURSOR_INFO cursor_info = {1, 0};  // 第二个值为0表示隐藏光标
    SetConsoleCursorInfo(GetStdHandle(STD_OUTPUT_HANDLE), &cursor_info);
}


void startup() // 数据初始化
{
    int i,j;

    //边框
    for(i=0;i<high;i++)
    {
        canvas[i][0]=-1;
        canvas[i][width-1]=-1;
        
    }
    for(j=0;j<width;j++)
    {
        canvas[0][j]=-1;
        canvas[high-1][j]=-1;
        
    }
    //蛇身
    canvas[high/2][width/2]=1;

    for (i=0;i<4;i++)
    {
        canvas[high/2][width/2-i]=1+i;
    }
 
    movedirection=4;

    food_x=rand()%(high-5)+2;
    food_y=rand()%(width-6)+3;
    canvas[food_x][food_y]=-2;


    
    HideCursor(); // 隐藏光标
}

void movesnakedirection()
{
    int i,j;
    int max=0;
    int oldtail_i,oldtail_j;//旧的蛇尾位置
    int oldhead_i,oldhead_j;//旧的蛇头位置

    for (i=1;i<high-1;i++)
        for (j=1;j<width-1;j++)
        {
            
            if(canvas[i][j]>0)
            {    //对所有大于1的元素加一
                canvas[i][j]++;
                //qiu max
                if (max<canvas[i][j])
                {
                    max=canvas[i][j];
                    oldtail_i=i;
                    oldtail_j=j;
                }
                if (canvas[i][j]==2)
                {
                    oldhead_i=i;
                    oldhead_j=j;
                }


            }
        }
    

        


            int newhand_i,newhand_j;
        if(movedirection==1)//1向上
        {
            
            newhand_i=oldhead_i-1;
            newhand_j=oldhead_j;
        }
        if(movedirection==2)//1向下
        {    
            newhand_i=oldhead_i+1;
            newhand_j=oldhead_j;
        }
        if(movedirection==3)//1向左
        {    
            newhand_i=oldhead_i;
            newhand_j=oldhead_j-1;
        }
        if(movedirection==4)//1向右
        {
            newhand_i=oldhead_i;
            newhand_j=oldhead_j+1;
        }
        if (canvas[newhand_i][newhand_j]==-2)
        {    
                canvas[food_x][food_y]=0;
                food_x=rand()%(high-5)+2;
                food_y=rand()%(width-6)+3;
                canvas[food_x][food_y]=-2;


        }
        else
            canvas[oldtail_i][oldtail_j]=0;



    if (canvas[newhand_i][newhand_j]>0 || canvas[newhand_i][newhand_j]==-1 )
    {
        printf("游戏失败！\n");
        exit(0);
    }
    else
    {
    
    canvas[newhand_i][newhand_j]=1;
    }
        

}



void show()  // 显示画面
{
     gotoxy(0,0);  // 光标移动到原点位置，以下重画清屏
    int i,j;
    for (i=0;i<high;i++)
    {
        for (j=0;j<width  ;j++)
        {
            if(canvas[i][j]==0)
                printf(" ");
            else if(canvas[i][j]==-1)
                printf("#");//输出边框
            else if(canvas[i][j]==1)
                printf("@");//输出头
            else if(canvas[i][j]>1)
                printf("*");//输出蛇身
            else if(canvas[i][j]==-2)
                printf("F");//输出food
        }
        printf("\n");
    }
    
    
}    



void updateWithoutInput()  // 与用户输入无关的更新
{
    movesnakedirection();

    Sleep(200);
}



void updateWithInput()  // 与用户输入有关的更新
{   
        char input;
    if(kbhit())  // 判断是否有输入
    {
        input = getch();  // 根据用户的不同输入来移动，不必输入回车
        if (input == 'a')   
        movedirection=3;
        if (input == 'd')
        movedirection=4;
        if (input == 'w')
        movedirection=1;
        if (input == 's')
        movedirection=2;
        if(input==27)
        {
        for(;;)
        {
                if(getch()==27)
                    break;
        }
        
        }
        
    }
    
}

int main()
{
    startup();  // 数据初始化    
    while (1) //  游戏循环执行
    {
        show();  // 显示画面
        updateWithoutInput();  // 与用户输入无关的更新
        updateWithInput();  // 与用户输入有关的更新
    }
    return 0;
}